import java.util.Scanner;

class LinearSearch
{
  public static void main(String args[])
  {
    int i, n, key, array[];

    Scanner in = new Scanner(System.in);
    System.out.println("Enter the number of elements of array");
    n = in.nextInt();
    array = new int[n];

    System.out.println("Enter " + n + " integers");

    for (i = 0; i < n; i++)
      array[i] = in.nextInt();

    System.out.println("Enter the key elements");
    key = in.nextInt();

    for (i = 0; i < n; i++)
    {
      if (array[i] == key)     // key is present
      {
         System.out.println(key + " is present at position " + (i + 1) + ".");
          break;
      }
   }
   if (i == n)  // key is absent
      System.out.println(key + " is not present in the array.");
  }
}